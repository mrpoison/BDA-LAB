import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Birinci ədədi daxil edin: ");
        double num1 = scanner.nextDouble();
        System.out.print("İkinci ədədi daxil edin: ");
        double num2 = scanner.nextDouble();
        System.out.print("Hər hansı bir əməliyyatı seçin (+, -, *, /): ");
        char operator = scanner.next().charAt(0);

        double result;

        switch(operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                if (num2 == 0) {
                    System.out.println("0-a bölmə yoxdur");
                    return;
                } else {
                    result = num1 / num2;
                    break;
                }
            default:
                System.out.println("xəta");
                return;
        }

        System.out.println(num1 + " " + operator + " " + num2 + " = " + result);
    }
}